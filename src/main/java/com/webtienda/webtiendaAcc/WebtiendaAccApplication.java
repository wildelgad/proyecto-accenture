package com.webtienda.webtiendaAcc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebtiendaAccApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebtiendaAccApplication.class, args);
	}

}
