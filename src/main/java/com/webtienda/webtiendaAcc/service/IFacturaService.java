package com.webtienda.webtiendaAcc.service;

import java.util.Optional;

import com.webtienda.webtiendaAcc.model.Factura;


public interface IFacturaService{
	public Iterable<Factura> findAll();
	public Optional<Factura> findById(Long id);
	public Factura save(Factura factura);
	public void deleteById(long id);
	//public Optional<Factura> findCedula(String Cedula);

}
