package com.webtienda.webtiendaAcc.service;


import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webtienda.webtiendaAcc.model.ProductoHasFactura;
import com.webtienda.webtiendaAcc.repository.ProductoHasFacturaRepository;


@Service
public  class ProductoHasFacturaService implements IProductoHasFacturaService {
	
	@Autowired
	private ProductoHasFacturaRepository productoHasPedidoRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<ProductoHasFactura> findAll() {
		return productoHasPedidoRepository.findAll();
	}
	@Override
	public Optional<ProductoHasFactura> findById(Long id) {
		return productoHasPedidoRepository.findById(id);
	}

	@Override
	public ProductoHasFactura save(ProductoHasFactura productoHasPedido) {
		return productoHasPedidoRepository.save(productoHasPedido);
	}

	@Override
	public void deleteById(long id) {
		productoHasPedidoRepository.deleteById(id);
		
	}

}
	










