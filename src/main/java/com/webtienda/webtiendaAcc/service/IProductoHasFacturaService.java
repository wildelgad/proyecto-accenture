package com.webtienda.webtiendaAcc.service;

import java.util.Optional;

import com.webtienda.webtiendaAcc.model.ProductoHasFactura;


public interface IProductoHasFacturaService {
	public Iterable<ProductoHasFactura> findAll();
	public Optional<ProductoHasFactura> findById(Long id);
	public ProductoHasFactura save( ProductoHasFactura productoHasPedido);
	public void deleteById(long id);

}