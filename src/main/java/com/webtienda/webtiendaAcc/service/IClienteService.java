package com.webtienda.webtiendaAcc.service;


import java.util.Optional;

import com.webtienda.webtiendaAcc.model.Cliente;


public interface IClienteService {
	public Iterable<Cliente> findAll();
	public Optional<Cliente> findById(Long id);
	public Cliente save(Cliente cliente);
	public void deleteById(long id);
	public Optional<Cliente> findCedula(String Cedula);

}