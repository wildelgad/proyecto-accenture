package com.webtienda.webtiendaAcc.service;

import java.util.Optional;

import com.webtienda.webtiendaAcc.model.Producto;


public interface  IProductoService{
	public Iterable<Producto> findAll();
	public Optional<Producto> findById(Long id);
	public Producto save(Producto producto);
	public void deleteById(long id);
	//public Optional<Producto> findCedula(String Cedula);

}
