package com.webtienda.webtiendaAcc.service;



import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webtienda.webtiendaAcc.model.Factura;
import com.webtienda.webtiendaAcc.repository.FacturaRepository;


@Service
public  class FacturaService implements IFacturaService {
	
	@Autowired
	private FacturaRepository facturaRepository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Factura> findAll() {
		return facturaRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Factura> findById(Long id) {
		return facturaRepository.findById(id);
	}

	@Override
	@Transactional
	public Factura save(Factura factura) {
		return facturaRepository.save(factura);
	}

	@Override
	@Transactional
	public void deleteById(long id) {
		facturaRepository.deleteById(id);
	}


}




