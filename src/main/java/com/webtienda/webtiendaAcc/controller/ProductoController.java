package com.webtienda.webtiendaAcc.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.webtienda.webtiendaAcc.model.Producto;
import com.webtienda.webtiendaAcc.service.ProductoService;


@RestController
@RequestMapping("/api/producto")
public class ProductoController  {
	
	@Autowired
	private ProductoService productoService;
	
	//Create a new user
	@PostMapping
	public ResponseEntity<?> create (@RequestBody Producto producto){
		return ResponseEntity.status(HttpStatus.CREATED).body(productoService.save(producto));
	}
	//Read user
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value = "id") Long ProductoId){
		Optional<Producto> oProducto = productoService.findById(ProductoId);
		System.out.println("hola antes del if");
		if(!oProducto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		System.out.println("hola antes despues if");
		return ResponseEntity.ok(oProducto);
	}
	
	//update an User
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Producto productoDetails, @PathVariable(value = "id") Long productoId){
		Optional <Producto> producto = productoService.findById(productoId);
		if(!producto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		//BeanUtils.copyProperties(userDetails, user.get()); este modifica todo incluido el id 
		
		producto.get().setNombre(productoDetails.getNombre());
		producto.get().setPrecio(productoDetails.getPrecio());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(productoService.save(producto.get()));
	}
}