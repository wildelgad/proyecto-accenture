package com.webtienda.webtiendaAcc.model;



import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Producto_has_Factura")
public class ProductoHasFactura implements Serializable{

	private static final long serialVersionUID = -710598978L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cantidad",nullable = false)
	private int  cantidad;
	
	@Column(name = "valor_total",nullable = false, length = 50)
	private int valor_total;
	

	@ManyToOne
	@JoinColumn(name="Producto_idProducto")
	private Producto producto_idProducto;
	
	@ManyToOne
	@JoinColumn(name="Factura_idFactura")
	private Factura factura_idFactura;

	public Long getIdProducto_has_Facturacol() {
		return id;
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getValor_total() {
		return valor_total;
	}

	public void setValor_total(int valor_total) {
		this.valor_total = valor_total;
	}

	public Producto getProducto_idProducto() {
		return producto_idProducto;
	}

	public void setProducto_idProducto(Producto producto_idProducto) {
		this.producto_idProducto = producto_idProducto;
	}

	public Factura getFactura_idFactura() {
		return factura_idFactura;
	}

	public void setFactura_idFactura(Factura factura_idFactura) {
		this.factura_idFactura = factura_idFactura;
	}


}




