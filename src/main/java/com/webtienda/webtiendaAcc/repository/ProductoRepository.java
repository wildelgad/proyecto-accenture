package com.webtienda.webtiendaAcc.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.webtienda.webtiendaAcc.model.Producto;


@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {
	
}