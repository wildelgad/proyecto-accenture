package com.webtienda.webtiendaAcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.webtienda.webtiendaAcc.model.ProductoHasFactura;


@Repository
public interface ProductoHasFacturaRepository extends JpaRepository<ProductoHasFactura, Long> {
	

}
