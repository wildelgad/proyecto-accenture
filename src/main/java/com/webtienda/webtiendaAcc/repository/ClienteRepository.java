package com.webtienda.webtiendaAcc.repository;


import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.webtienda.webtiendaAcc.model.Cliente;
//import com.pruebaTiendaOnline.entity.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	Optional<Cliente> findByCedula(String Cedula);

}